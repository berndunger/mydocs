---
title: "Startseite"
date: 2022-10-22
draft: false
menu:
  main:
    identifier: Startseite
    name: Startseite
    weight: 100
---
:toc:
:toclevels: 1

== Willkommen 

auf meiner Dokumentationssite +

Ich dokumentiere hier IT-Themen, und wer mitlesen möchte, ist gerne willkommen.

== Programmiersprachen

Ich entwickle im allg. mit Java (+Vaadin, + Mssql, + mybatis),
also dreht sich das meisste um diese Programmiersprache.
Als IDE benutze ich Netbeans, versuche die Idea zu benutzen, kann mich aber nicht so recht von Netbeans lösen.

Java deswegen, da viele Dinge schon während des Compilierens abgefangen werden, 
ein riesigen Ökosystem vorhanden ist und die Zukunftssicherheit auch deswegen gegeben ist. +
Typsicherheit kann man Flexibilität gegenüberstellen,
aber unter Zeitdruck begeht man schon mal Fehler, 
es sei denn man ist ein genialer fehlerfrei arbeitender Kopf oder ein Manager.
Und prägnanter Code mag toll sein, ein paar mehr Codezeilen kommentieren ggf. besser die Absichten und lassen sich ggf. auch besser debuggen.

== Site

Die Site wird mit Hugo generiert, unter eine minimalen Verwendung von Javascript.
Ziel war ein Theme zu erstellen, bei dem man nicht jeden Woche hinterherpatchen muß,
weil irgendeine Library eine Sicherheitslücke aufweist.

== Rechentechnik und Freiheit

Privat bevorzuge ich: +

* Linux als Desktop, Windows ist mir zu wissbegierig und dann bin ich lieber Kommunist.
* LineageOS statt Android, ich mag einfach nicht, wenn mir irgendwann irgendwie irgendjemand eine Überwachung in das Telefon patcht (obwohl, die ist ja bei Android so schon enthalten) . +

Interessanterweise juckt die Übergriffigkeit von Android oder Windows keinen unserer Bürgerrechtler,
also jene Leute, die 1989 tapfer "Stasi in die Produktion" gebrüllt haben und noch heute bei jeder Gelegenheit wg. der Überwachung durch die Staatssicherheit rumheulen. +
Das wir uns richtig verstehen, privat ist privat, und ich heiße die Ausschnüffelung von Bürgern nicht gut,
aber Privatsphäre ist auch in "Freiheit" zu respektieren und nicht nur im Sozialismus.
Und genau deswegen sollte man nicht nur mit dem Finger auf China zeigen, sondern gerade auch vor der eigenen Haustür kehren, also in den USA und innerhalb der EU. + 

Ansonsten sehr lesenswert zum Thema: link:https://www.kuketz-blog.de/[]

Leider gibt es Bestrebungen erweiterter Überwachung auch innerhalb der EU (Stasi-Uschi), angeblich um solche Dinge wie Geldwäsche zu unterbinden und die Mafia zu bekämpfen.
Toll, nur warum stattet man die Finanzämter erst mal vernünftig aus, damit die ihrer Arbeit nachkommen können und setzt die Gesetze auch bei den Reichen durch ? Stimmt, unser Kanzler hatte da mal was vergessen. 

