---
title: Docker
date: "2022-10-22"
draft: false
description: "Docker Cheatsheet"
tags: ["docker"]
menu:
  main:
    identifier: docker
    name: docker
    weight: 800
---
:toc:
:toclevels: 1

Wichtige Kommandos von Docker komprimiert.

== Docker Compose

link:https://docs.docker.com/engine/reference/commandline/compose/[]

[%header,cols="1,2"]
|===

| Kommando
| Beschreibung

| docker compose up -d
| startet eine Applikation entstprechen der im aktuellen Pfad vorhandenen docker-compose.yml

| docker compose rm
| entfernt eine Applikation

| docker compose start/stop/restart
| startet/stop/restartet eine Applikation, baut die aber nicht neu

| docker compose logs
| Zeigt die Docker logs für eine Applikation an

| docker compose top/ls
| Informationen zu den Containern anzeigen

|===

Standardmäßig wird die Datei docker-compose.yml im aktuellen Verzeichnis verwendet. Ein abweichendes Konfigurationsfile kann mit dem Schalter -f angegeben werden.

