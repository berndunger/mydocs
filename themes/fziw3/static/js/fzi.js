 
 function hideShowMainMenu() {
        var x = document.getElementById("mainMenu");
        if (x.className.indexOf("w3-show") == -1) {
          x.className += " w3-show";
          x.className = x.className.replace(" w3-hide", "");
        } else {
          x.className = x.className.replace(" w3-show", "");
          x.className += " w3-hide";
        }
      }

         function hideShowSectionMenu() {
        var x = document.getElementById("sectionMenu");
        if (x.className.indexOf("w3-show") == -1) {
          x.className += " w3-show";
          x.className = x.className.replace(" w3-hide", "");
        } else {
          x.className = x.className.replace(" w3-show", "");
          x.className += " w3-hide";
        }
      }

      <!-- Zeigt einen Unterpunkt im Sectionmenu an  -->
      function myAccFunc(id) {
        var x = document.getElementById(id);
        var y = document.getElementById("s".concat(id));

        if (x.className.indexOf("w3-show") == -1) {
          x.className += " w3-show";
          x.previousElementSibling.className += " w3-red";
          y.className = "mdi mdi-menu-down"
        } else {
          x.className = x.className.replace(" w3-show", "");
          x.previousElementSibling.className =
          x.previousElementSibling.className.replace(" w3-red", "");
          y.className = "mdi mdi-menu-right"
        }
      }
